<?php

namespace App\Service;

use App\Entity\Tile;
use App\Repository\TileRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class TileManager
 * @package App\Service
 */
class TileManager extends BaseManager
{
    /** @var CategoryManager */
    private $categoryManager;

    public function __construct(
        EntityManagerInterface $objectManager,
        CategoryManager $categoryManager
    ) {
        parent::__construct($objectManager);

        $this->categoryManager = $categoryManager;
    }

    /**
     * @param string $entityName
     * @return BaseManager
     * @required
     */
    public function setEntityName(string $entityName = Tile::class): BaseManager
    {
        return parent::setEntityName($entityName);
    }

    /**
     * @param string $page
     * @return Tile[]
     */
    public function getAllFromPage(string $page): array
    {
        /** @var TileRepository $rTile */
        $rTile = $this->getRepository();

        return $rTile->findAllFromPage($page);
    }

    /**
     * @param string $title
     * @return array
     */
    public function getByTitle(string $title): array
    {
        /** @var TileRepository $rTile */
        $rTile = $this->getRepository();

        return $rTile->findByTitle($title);
    }

    /**
     * @return array
     */
    public function getAllOrdered(): array
    {
        return $this->getBy([], ['id' => 'DESC']);
    }

    /**
     * @param string $name
     * @return array
     */
    public function getByCategoryName(string $name): array
    {
        return $this->getBy(['category' => $this->categoryManager->getOneBy(['name' => $name])]);
    }
}
