<?php

namespace App\Controller;

use App\Entity\Page;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MusicPageController
 * @package App\Controller
 */
class MusicPageController extends PaginatedTilesController
{
    /**
     * @Route("/music/{page}", name="music_page", requirements={"page"="\d+"})
     * @param Request $request
     * @param string $view
     * @param string $pageName
     * @param int $page
     * @param bool $isRendered
     * @return Response
     */
    public function index(
        Request $request,
        string $view = 'music_page/index.html.twig',
        string $pageName = Page::PAGE_MUSIC,
        int $page = 1,
        bool $isRendered = true
    ): Response {
        return parent::index($request, $view, $pageName, $page, $isRendered);
    }
}
