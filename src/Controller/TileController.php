<?php

namespace App\Controller;

use App\Entity\Tile;
use App\Form\TileType;
use App\Service\CategoryManager;
use App\Service\PageManager;
use App\Service\TileManager;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class TileController extends PaginatedTilesController
{
    /** @var PageManager */
    private $pageManager;

    /**
     * TileController constructor.
     * @param PaginatorInterface $paginator
     * @param TileManager $tileManager
     * @param PageManager $pageManager
     * @param CategoryManager $categoryManager
     */
    public function __construct(
        PaginatorInterface $paginator,
        TileManager $tileManager,
        PageManager $pageManager,
        CategoryManager $categoryManager
    ) {
        parent::__construct($paginator, $tileManager, $categoryManager);

        $this->pageManager = $pageManager;
    }

    /**
     * @Route("/{page}", name="admin_tile_index", methods={"GET"}, requirements={"page"="\d+"})
     * @param Request $request
     * @param string $view
     * @param string $pageName
     * @param int $page
     * @param bool $isRendered
     * @return Response
     */
    public function index(
        Request $request,
        string $view = 'tile/index.html.twig',
        string $pageName = '',
        int $page = 1,
        bool $isRendered = true
    ): Response {
       return parent::index($request, $view, $pageName, $page, $isRendered);
    }

    /**
     * @Route("/new", name="admin_tile_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function new(Request $request): Response
    {
        $tile = new Tile();
        $form = $this->createForm(TileType::class, $tile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addClassToIframe($tile);
            $this->tileManager->create($tile);

            return $this->redirectToRoute('admin_tile_index');
        }

        return $this->render('tile/new.html.twig', [
            'tile' => $tile,
            'pages' => $this->pageManager->getPageForTileType(),
            'categories' => $this->categoryManager->getAllForTileType(),
            'form' => $form->createView(),
            'image' => Tile::TILE_IMAGE,
            'player' => Tile::TILE_PLAYER,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_tile_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Tile $tile
     * @return Response
     */
    public function edit(Request $request, Tile $tile): Response
    {
        $form = $this->createForm(TileType::class, $tile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addClassToIframe($tile);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_tile_index');
        }

        return $this->render('tile/edit.html.twig', [
            'tile' => $tile,
            'pages' => $this->pageManager->getPageForTileType(),
            'categories' => $this->categoryManager->getAllForTileType(),
            'form' => $form->createView(),
            'image' => Tile::TILE_IMAGE,
            'player' => Tile::TILE_PLAYER,
        ]);
    }

    /**
     * @Route("/{id}", name="admin_tile_delete", methods={"DELETE"})
     * @param Request $request
     * @param Tile $tile
     * @return Response
     * @throws Exception
     */
    public function delete(Request $request, Tile $tile): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tile->getId(), $request->request->get('_token'))) {
            $this->tileManager->delete($tile);
        }

        return $this->redirectToRoute('admin_tile_index');
    }

    /**
     * @param Tile $tile
     */
    private function addClassToIframe(Tile $tile): void
    {
        if (Tile::TILE_PLAYER === $tile->getType() && stripos($tile->getContent(), '<iframe ') !== false) {
            $tile->setContent(substr_replace($tile->getContent(), ' class="has-ratio"', 7, 0));
        }
    }
}
