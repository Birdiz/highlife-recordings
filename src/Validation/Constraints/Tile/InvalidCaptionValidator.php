<?php

namespace App\Validation\Constraints\Tile;

use App\Entity\Page;
use App\Entity\Tile;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
 * Class InvalidCaptionValidator
 * @package App\Validation\Constraints\Tile
 */
class InvalidCaptionValidator extends ConstraintValidator
{
    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof InvalidCaption) {
            throw new UnexpectedTypeException($constraint, InvalidCaption::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (empty($value) || !$value instanceof Tile) {
            return;
        }

        if (!empty($value->getCaption()) &&
            !in_array($value->getPage()->getName(), Page::getPagesCaptionable(), true)
        ) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
