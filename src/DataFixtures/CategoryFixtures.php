<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    private const CATEGORIES = [
        [
            'name' => Category::CORPORATE,
        ],
        [
            'name' => Category::CULTURE,
        ],
        [
            'name' => Category::LUXE,
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach (self::CATEGORIES as $categoryArray) {
            $category = (new Category())
                ->setName($categoryArray['name']);

            $manager->persist($category);
        }
        $manager->flush();
    }
}
