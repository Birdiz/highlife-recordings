<?php

namespace App\Controller;

use App\Entity\Page;
use App\Service\PageManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContactPageController
 * @package App\Controller
 */
class ContactPageController extends AbstractController
{
    /** @var PageManager */
    private $pageManager;

    /**
     * ContactPageController constructor.
     * @param PageManager $pageManager
     */
    public function __construct(PageManager $pageManager)
    {
        $this->pageManager = $pageManager;
    }

    /**
     * @Route("/contact", name="contact_page")
     */
    public function index(): Response
    {
        /** @var Page $page */
        $page = $this->pageManager->getOneBy(['name' => Page::PAGE_CONTACT]);

        return $this->render('contact_page/index.html.twig', [
            'content' => $page->getContent(),
        ]);
    }
}
