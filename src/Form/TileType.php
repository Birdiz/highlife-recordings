<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Page;
use App\Entity\Tile;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TileType
 * @package App\Form
 */
class TileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class)
            ->add('content', TextType::class)
            ->add('caption', TextareaType::class, ['required' => false])
            ->add('link', TextType::class, ['required' => false])
            ->add('type', ChoiceType::class, ['choices' => Tile::getTypes()])
            ->add(
                'page',
                EntityType::class,
                [
                    'class' => Page::class,
                    'choice_label' => 'name',
                ]
            )
            ->add(
                'category',
                EntityType::class,
                [
                    'required' => false,
                    'class' => Category::class,
                    'empty_data' => null,
                    'choice_label' => 'name',
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Tile::class,
        ]);
    }
}
