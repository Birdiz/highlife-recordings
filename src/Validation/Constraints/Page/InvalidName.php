<?php

namespace App\Validation\Constraints\Page;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class InvalidName extends Constraint
{
    public $message = 'The page\'s name does not exist';
}
