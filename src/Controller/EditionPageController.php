<?php

namespace App\Controller;

use App\Entity\Page;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class EditionPageController
 * @package App\Controller
 */
class EditionPageController extends PaginatedTilesController
{
    /**
     * @Route("/edition/{page}", name="edition_page", requirements={"page"="\d+"})
     * @param Request $request
     * @param string $view
     * @param string $pageName
     * @param int $page
     * @param bool $isRendered
     * @return Response
     */
    public function index(
        Request $request,
        string $view = 'edition_page/index.html.twig',
        string $pageName = Page::PAGE_EDITION,
        int $page = 1,
        bool $isRendered = true
    ): Response {
        return parent::index($request, $view, $pageName, $page, $isRendered);
    }
}
