<?php

namespace App\Event;

/**
 * Class EntityEvents
 * @package App\Event
 */
final class EntityEvents
{
    /**
     * Triggered before a entity is persist.
     *
     * @Event("App\Event\EntityEvent")
     */
    public const PATTERN_PRE_CREATE = 'resource.%s.pre_create';

    /**
     * Triggered after a entity is persist.
     *
     * @Event("App\Event\EntityEvent")
     */
    public const PATTERN_POST_CREATE = 'resource.%s.post_create';

    /**
     * Triggered before a entity is update.
     *
     * @Event("App\Event\EntityEvent")
     */
    public const PATTERN_PRE_UPDATE = 'resource.%s.pre_update';

    /**
     * Triggered after a entity is update.
     *
     * @Event("App\Event\EntityEvent")
     */
    public const PATTERN_POST_UPDATE = 'resource.%s.post_update';

    /**
     * Triggered before a entity is remove.
     *
     * @Event("App\Event\EntityEvent")
     */
    public const PATTERN_PRE_DELETE = 'resource.%s.pre_delete';

    /**
     * Triggered after a entity is remove.
     *
     * @Event("App\Event\EntityEvent")
     */
    public const PATTERN_POST_DELETE = 'resource.%s.post_delete';
}
