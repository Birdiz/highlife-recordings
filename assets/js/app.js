require('../css/app.css');
let logoPath = require('../img/logo.jpg');
let $ = require('jquery');

$(document).ready(function() {
  $('#header-logo').attr('src', logoPath);
  showTileCaption();
  showTileLink();
  showTileCategory();
});

function showTileCaption() {
  let $select = $('#tile_page');
  let $captionWrapper = $('#tile_caption_wrapper');

  $captionWrapper.hide();
  if ($select.val() === "1" || $select.val() === "4" || $select.val() === "7") {
    $captionWrapper.show();
  }

  $select.change(function () {
    if ($select.val() === "1" || $select.val() === "4" || $select.val() === "7") {
      $captionWrapper.show();
    } else {
      $captionWrapper.hide();
      tinymce.activeEditor.setContent('');
    }
  });
}

function showTileLink() {
  let $select = $('#tile_type');
  let $linkWrapper = $('#tile_link_wrapper');

  $linkWrapper.hide();
  if ($select.val() === "image") {
    $linkWrapper.show();
  }

  $select.change(function () {
    if ($select.val() === "image") {
      $linkWrapper.show();
    } else {
      $linkWrapper.hide();
      $('#tile_link').val('')
    }
  });
}

function showTileCategory() {
  let $select = $('#tile_page');
  let $categoryWrapper = $('#tile_category_wrapper');

  $categoryWrapper.hide();
  if ($select.val() === "4") {
    $categoryWrapper.show();
  }

  $select.change(function () {
    if ($select.val() === "4") {
      $categoryWrapper.show();
    } else {
      $categoryWrapper.hide();
      $('#tile_category').val(null)
    }
  });
}
