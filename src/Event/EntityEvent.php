<?php

namespace App\Event;

use App\Contract\EntityInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class EntityEvent
 * @package App\Event
 */
class EntityEvent extends Event
{
    /** @var EntityInterface */
    private $object;

    /**
     * EntityEvent constructor.
     *
     * @param $object
     */
    public function __construct(EntityInterface $object)
    {
        $this->object = $object;
    }

    /**
     * @return EntityInterface
     */
    public function getObject(): EntityInterface
    {
        return $this->object;
    }
}
