<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    public const CORPORATE = 'Corporate';
    public const CULTURE = 'Culture';
    public const LUXE = 'Luxe';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tile", mappedBy="category")
     */
    private $tiles;

    /**
     * Category constructor.
     */
    public function __construct()
    {
        $this->tiles = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Category
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Tile[]
     */
    public function getTiles(): Collection
    {
        return $this->tiles;
    }

    /**
     * @param Tile $tile
     * @return Category
     */
    public function addTile(Tile $tile): self
    {
        if (!$this->tiles->contains($tile)) {
            $this->tiles[] = $tile;
            $tile->setCategory($this);
        }

        return $this;
    }

    /**
     * @param Tile $tile
     * @return Category
     */
    public function removeTile(Tile $tile): self
    {
        if ($this->tiles->contains($tile)) {
            $this->tiles->removeElement($tile);
            // set the owning side to null (unless already changed)
            if ($tile->getCategory() === $this) {
                $tile->setCategory(null);
            }
        }

        return $this;
    }
}
