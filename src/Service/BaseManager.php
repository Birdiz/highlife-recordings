<?php

namespace App\Service;

use App\Contract\BaseManagerInterface;
use App\Contract\EntityInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;
use RuntimeException;

class BaseManager implements BaseManagerInterface
{
    /** @var EntityManagerInterface */
    protected $objectManager;

    /** @var string */
    protected $entityName;

    /**
     * BaseManager constructor.
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Create entity.
     *
     * @param EntityInterface $entity
     *
     * @return EntityInterface
     *
     * @throws Exception
     */
    public function create(EntityInterface $entity): EntityInterface
    {
        $this->preventBadEntityType($entity);
        $this->preventEntityState($entity, 'Creation impossible, entity already exist.', true);

        return $this->save($entity);
    }

    /**
     * Update entity.
     *
     * @param EntityInterface $entity
     *
     * @return EntityInterface
     *
     * @throws Exception
     */
    public function update(EntityInterface $entity): EntityInterface
    {
        $this->preventBadEntityType($entity);
        $this->preventEntityState($entity, 'Update impossible, entity doesn\'t exist.', false);

        return $this->save($entity);
    }

    /**
     * @param EntityInterface $entity
     *
     * @throws Exception
     */
    public function delete(EntityInterface $entity): void
    {
        $this->preventBadEntityType($entity);
        $this->preventEntityState($entity, 'Delete impossible, entity doesn\'t exist.', false);
        try {
            $this->objectManager->remove($entity);
            $this->objectManager->flush();
        } catch (Exception $e) {
            throw new RuntimeException(
                sprintf(
                    'An error occurs on delete %s #%d entity',
                    $this->entityName,
                    $entity->getId()
                ),
                $e->getCode(),
                $e
            );
        }
    }

    /**
     * Get object by id.
     *
     * @param int $id
     * @return EntityInterface t
     */
    public function get(int $id): EntityInterface
    {
        /** @var EntityInterface $entity */
        $entity = $this->getRepository()->find($id);

        if (null === $entity) {
            throw new RuntimeException(
                sprintf(
                    'Entity #%d of "%s" type not found',
                    $id,
                    $this->entityName
                ),
                404
            );
        }

        return $entity;
    }

    /**
     * Return all objects.
     *
     * @return object[]
     */
    public function getAll(): array
    {
        return $this->getRepository()->findAll();
    }

    /**
     * @param array $criteria
     * @return mixed
     */
    public function getOneBy(array $criteria)
    {
        return $this->getRepository()->findOneBy($criteria);
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getBy(array $criteria, ?array $orderBy = null, int $limit = null, int $offset = null): array
    {
        return $this->getRepository()->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * Defined the default entity name.
     *
     * @param string $entityName
     * @return $this
     */
    public function setEntityName(string $entityName): BaseManager
    {
        $this->entityName = $entityName;

        return $this;
    }

    /**
     * Get repository.
     *
     * @return ObjectRepository
     */
    public function getRepository(): ObjectRepository
    {
        return $this->objectManager->getRepository($this->entityName);
    }

    /**
     * @param EntityInterface $entity
     * @return EntityInterface
     */
    private function save(EntityInterface $entity): EntityInterface
    {
        try {
            $this->objectManager->persist($entity);
            $this->objectManager->flush();
        } catch (Exception $e) {
            throw new RuntimeException(
                sprintf(
                    'On error occurs on save "%s" #%d entity',
                    $this->entityName,
                    $entity->getId()
                ),
                $e->getCode(),
                $e
            );
        }

        return $entity;
    }

    /**
     * @return string
     */
    private function getEntityAlias(): string
    {
        $match = explode('\\', $this->entityName);

        return strtolower(end($match));
    }

    /**
     * @param EntityInterface $entity
     */
    private function preventBadEntityType(EntityInterface $entity): void
    {
        if (!$entity instanceof $this->entityName) {
            throw new InvalidArgumentException(
                sprintf(
                    'Bad entity type, "%s" given instead of "%s"',
                    get_class($entity),
                    $this->entityName
                )
            );
        }
    }

    /**
     * @param EntityInterface $entity
     * @param string          $msg
     * @param bool            $exist
     *
     * @throws Exception
     */
    private function preventEntityState(EntityInterface $entity, string $msg, bool $exist): void
    {
        if ($exist xor !$entity->getId()) {
            throw new RuntimeException($msg);
        }
    }
}
