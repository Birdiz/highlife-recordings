<?php

namespace App\Repository;

use App\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageRepository extends ServiceEntityRepository
{
    /**
     * PageRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    /**
     * @return array
     */
    public function findAllEditable(): array
    {
        return $this
            ->createQueryBuilder('p')
            ->andWhere('p.name IN (:names)')
            ->setParameter('names', Page::getPagesEditable())
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array
     */
    public function getPageForTileType(): array
    {
        return $this
            ->createQueryBuilder('p')
            ->andWhere('p.name in (:names)')
            ->setParameter('names', Page::getNamesForTile())
            ->orderBy('p.id')
            ->getQuery()
            ->getResult();
    }
}
