<?php

namespace App\Validation\Constraints\Tile;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class InvalidCaption extends Constraint
{
    public $message = 'The caption must be for the home page or the edition page';

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
}
