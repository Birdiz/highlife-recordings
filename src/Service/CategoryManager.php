<?php

namespace App\Service;

use App\Entity\Category;

/**
 * Class CategoryManager
 * @package App\Service
 */
class CategoryManager extends BaseManager
{
    /**
     * @param string $entityName
     * @return BaseManager
     * @required
     */
    public function setEntityName(string $entityName = Category::class): BaseManager
    {
        return parent::setEntityName($entityName);
    }

    /**
     * @return array
     */
    public function getAllForTileType(): array
    {
        $categories = $this->getAll();
        array_unshift($categories, null);

        return $categories;
    }
}
