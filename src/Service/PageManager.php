<?php

namespace App\Service;

use App\Entity\Page;
use App\Repository\PageRepository;

/**
 * Class PageManager
 * @package App\Service.
 */
class PageManager extends BaseManager
{
    /**
     * @param string $entityName
     * @return BaseManager
     * @required
     */
    public function setEntityName(string $entityName = Page::class): BaseManager
    {
        return parent::setEntityName($entityName);
    }

    /**
     * @return array
     */
    public function getAllEditable(): array
    {
        /** @var PageRepository $rPage */
        $rPage = $this->getRepository();

        return $rPage->findAllEditable();
    }

    /**
     * @return array
     */
    public function getPageForTileType(): array
    {
        /** @var PageRepository $rPage */
        $rPage = $this->getRepository();

        return $rPage->getPageForTileType();
    }
}
