<?php

namespace App\Controller;

use App\Entity\Page;
use App\Form\PageType;
use App\Repository\PageRepository;
use App\Service\PageManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class PageController extends AbstractController
{
    /** @var PageManager */
    private $pageManager;

    /**
     * PageController constructor.
     * @param PageManager $pageManager
     */
    public function __construct(PageManager $pageManager)
    {
        $this->pageManager = $pageManager;
    }

    /**
     * @Route("/page", name="admin_page_index", methods={"GET"})
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('page/index.html.twig', [
            'pages' => $this->pageManager->getAllEditable(),
        ]);
    }

    /**
     * @Route("/page/{id}/edit", name="admin_page_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Page $page
     * @return Response
     */
    public function edit(Request $request, Page $page): Response
    {
        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_page_index');
        }

        return $this->render('page/edit.html.twig', [
            'page' => $page,
            'form' => $form->createView(),
        ]);
    }
}
