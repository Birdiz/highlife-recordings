<?php

namespace App\DataFixtures;

use App\Entity\Categrory;
use App\Entity\Page;
use App\Entity\Tile;
use App\Service\PageManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TileFixtures extends Fixture
{
    private const TILES = [
        [
            'title' => 'Test 1',
            'content' => 'https://images.unsplash.com/photo-1473830394358-91588751b241?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
            'link' => '/',
            'type' => Tile::TILE_IMAGE,
            'page' => Page::PAGE_HOME,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 2',
            'content' => "<iframe class='has-ratio' width='640' height='360' src='https://www.youtube.com/embed/t99BfDnBZcI' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe class='has-ratio'>",
            'type' => Tile::TILE_PLAYER,
            'page' => Page::PAGE_HOME,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 3',
            'content' => 'https://images.unsplash.com/photo-1473830394358-91588751b241?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
            'link' => '/',
            'type' => Tile::TILE_IMAGE,
            'page' => Page::PAGE_HOME,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 4',
            'content' => 'https://images.unsplash.com/photo-1473830394358-91588751b241?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
            'link' => '/',
            'type' => Tile::TILE_IMAGE,
            'page' => Page::PAGE_HOME,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 11',
            'content' => 'https://images.unsplash.com/photo-1473830394358-91588751b241?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
            'link' => '/',
            'type' => Tile::TILE_IMAGE,
            'page' => Page::PAGE_HOME,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 12',
            'content' => "<iframe class='has-ratio' width='640' height='360' src='https://www.youtube.com/embed/t99BfDnBZcI' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe class='has-ratio'>",
            'type' => Tile::TILE_PLAYER,
            'page' => Page::PAGE_HOME,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 13',
            'content' => 'https://images.unsplash.com/photo-1473830394358-91588751b241?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
            'link' => '/',
            'type' => Tile::TILE_IMAGE,
            'page' => Page::PAGE_HOME,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 14',
            'content' => 'https://images.unsplash.com/photo-1473830394358-91588751b241?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
            'link' => '/',
            'type' => Tile::TILE_IMAGE,
            'page' => Page::PAGE_HOME,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 21',
            'content' => 'https://images.unsplash.com/photo-1473830394358-91588751b241?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
            'link' => '/',
            'type' => Tile::TILE_IMAGE,
            'page' => Page::PAGE_HOME,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 22',
            'content' => "<iframe class='has-ratio' width='640' height='360' src='https://www.youtube.com/embed/t99BfDnBZcI' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe class='has-ratio'>",
            'type' => Tile::TILE_PLAYER,
            'page' => Page::PAGE_HOME,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 23',
            'content' => 'https://images.unsplash.com/photo-1473830394358-91588751b241?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
            'link' => '/',
            'type' => Tile::TILE_IMAGE,
            'page' => Page::PAGE_HOME,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 24',
            'content' => 'https://images.unsplash.com/photo-1473830394358-91588751b241?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
            'link' => '/',
            'type' => Tile::TILE_IMAGE,
            'page' => Page::PAGE_HOME,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 5',
            'content' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/GedNgondoMessager.JPG/1200px-GedNgondoMessager.JPG',
            'link' => '/',
            'type' => Tile::TILE_IMAGE,
            'page' => Page::PAGE_EDITION,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 6',
            'content' => "<iframe class='has-ratio' width='640' height='360' src='https://www.youtube.com/embed/t99BfDnBZcI' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe class='has-ratio'>",
            'type' => Tile::TILE_PLAYER,
            'page' => Page::PAGE_EDITION,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 7',
            'content' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/GedNgondoMessager.JPG/1200px-GedNgondoMessager.JPG',
            'link' => '/',
            'type' => Tile::TILE_IMAGE,
            'page' => Page::PAGE_EDITION,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 8',
            'content' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/GedNgondoMessager.JPG/1200px-GedNgondoMessager.JPG',
            'link' => '/',
            'type' => Tile::TILE_IMAGE,
            'page' => Page::PAGE_EDITION,
            'caption' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc imperdiet, condimentum mauris rutrum, mollis odio. Aliquam id nisl nisl.'
        ],
        [
            'title' => 'Test 9',
            'content' => "<iframe class='has-ratio' width='640' height='360' src='https://www.youtube.com/embed/t99BfDnBZcI' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe class='has-ratio'>",
            'type' => Tile::TILE_PLAYER,
            'page' => Page::PAGE_RADIO,
        ],
        [
            'title' => 'Test 10',
            'content' => "<iframe class='has-ratio' width='640' height='360' src='https://www.youtube.com/embed/t99BfDnBZcI' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe class='has-ratio'>",
            'type' => Tile::TILE_PLAYER,
            'page' => Page::PAGE_RADIO,
        ],
        [
            'title' => 'Test 11',
            'content' => "<iframe class='has-ratio' width='640' height='360' src='https://www.youtube.com/embed/t99BfDnBZcI' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe class='has-ratio'>",
            'type' => Tile::TILE_PLAYER,
            'page' => Page::PAGE_RADIO,
        ],
        [
            'title' => 'Test 12',
            'content' => "<iframe class='has-ratio' width='640' height='360' src='https://www.youtube.com/embed/t99BfDnBZcI' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe class='has-ratio'>",
            'type' => Tile::TILE_PLAYER,
            'page' => Page::PAGE_RADIO,
        ],
        [
            'title' => 'Test 13',
            'content' => "<iframe class='has-ratio' width='640' height='360' src='https://www.youtube.com/embed/t99BfDnBZcI' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe class='has-ratio'>",
            'type' => Tile::TILE_PLAYER,
            'page' => Page::PAGE_MUSIC,
        ],
        [
            'title' => 'Test 14',
            'content' => "<iframe class='has-ratio' width='640' height='360' src='https://www.youtube.com/embed/t99BfDnBZcI' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe class='has-ratio'>",
            'type' => Tile::TILE_PLAYER,
            'page' => Page::PAGE_MUSIC,
        ],
        [
            'title' => 'Test 15',
            'content' => "<iframe class='has-ratio' width='640' height='360' src='https://www.youtube.com/embed/t99BfDnBZcI' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe class='has-ratio'>",
            'type' => Tile::TILE_PLAYER,
            'page' => Page::PAGE_MUSIC,
        ],
        [
            'title' => 'Test 16',
            'content' => "<iframe class='has-ratio' width='640' height='360' src='https://www.youtube.com/embed/t99BfDnBZcI' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe class='has-ratio'>",
            'type' => Tile::TILE_PLAYER,
            'page' => Page::PAGE_MUSIC,
        ],
    ];

    /** @var PageManager */
    private $pageManager;

    public function __construct(PageManager $pageManager)
    {
        $this->pageManager = $pageManager;
    }

    public function load(ObjectManager $manager): void
    {
        foreach (self::TILES as $tileArray) {
            $tile = (new Tile())
                ->setTitle($tileArray['title'])
                ->setContent($tileArray['content'])
                ->setType($tileArray['type']);

            if (isset($tileArray['link'])) {
                $tile->setLink($tileArray['link']);
            }
            if (isset($tileArray['caption'])) {
                $tile->setCaption($tileArray['caption']);
            }

            /** @var Page $page */
            $page = $this->pageManager->getOneBy(['name' => $tileArray['page']]);
            $tile->setPage($page);

            $manager->persist($tile);
        }
        $manager->flush();
    }
}
