<?php

namespace App\DataFixtures;

use App\Entity\Page;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PageFixtures extends Fixture
{
    private const PAGES = [
        [
            'name' => Page::PAGE_HOME,
        ],
        [
            'name' => Page::PAGE_ABOUT,
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at eros ipsum. Curabitur vel dolor sodales, porta ex in, finibus elit. Ut nibh lectus, varius in pretium id, interdum eu arcu. Curabitur venenatis sodales urna, at scelerisque elit imperdiet quis. Vivamus et venenatis lectus. Ut in massa id justo interdum congue in et odio. Nullam tincidunt est id consectetur imperdiet.',
        ],
        [
            'name' => Page::PAGE_MUSIC,
        ],
        [
            'name' => Page::PAGE_EDITION,
        ],
        [
            'name' => Page::PAGE_RADIO,
        ],
        [
            'name' => Page::PAGE_CONTACT,
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at eros ipsum. Curabitur vel dolor sodales, porta ex in, finibus elit. Ut nibh lectus, varius in pretium id, interdum eu arcu. Curabitur venenatis sodales urna, at scelerisque elit imperdiet quis. Vivamus et venenatis lectus. Ut in massa id justo interdum congue in et odio. Nullam tincidunt est id consectetur imperdiet.',
        ],
        [
            'name' => Page::PAGE_SERVICES,
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at eros ipsum. Curabitur vel dolor sodales, porta ex in, finibus elit. Ut nibh lectus, varius in pretium id, interdum eu arcu. Curabitur venenatis sodales urna, at scelerisque elit imperdiet quis. Vivamus et venenatis lectus. Ut in massa id justo interdum congue in et odio. Nullam tincidunt est id consectetur imperdiet.',
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach (self::PAGES as $pageArray) {
            $page = (new Page())
                ->setName($pageArray['name']);

            if (isset($pageArray['content'])) {
                $page->setContent($pageArray['content']);
            }

            $manager->persist($page);
        }
        $manager->flush();
    }
}
