<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    /** @var UserPasswordEncoderInterface */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        $admin = new User();

        $admin
            ->setUsername('highlife')
            ->setPassword($this->encoder->encodePassword($admin, 'highlife'))
            ->setEmail('test@email.com')
            ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
            ->setSuperAdmin(true)
            ->setEnabled(true);


        $manager->persist($admin);
        $manager->flush();
    }
}
