<?php

namespace App\Controller;

use App\Entity\Page;
use App\Entity\Tile;
use App\Service\CategoryManager;
use App\Service\TileManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PaginatedTilesController
 * @package App\Controller
 */
class PaginatedTilesController extends AbstractController
{
    /** @var PaginatorInterface */
    protected $paginator;

    /** @var TileManager */
    protected $tileManager;

    /** @var CategoryManager */
    protected $categoryManager;

    /**
     * PaginatedTilesController constructor.
     * @param PaginatorInterface $paginator
     * @param TileManager $tileManager
     * @param CategoryManager $categoryManager
     */
    public function __construct(
        PaginatorInterface $paginator,
        TileManager $tileManager,
        CategoryManager $categoryManager
    ) {
        $this->paginator = $paginator;
        $this->tileManager = $tileManager;
        $this->categoryManager = $categoryManager;
    }

    /**
     * @param Request $request
     * @param string $view
     * @param string $pageName
     * @param int $page
     * @param bool $isRendered
     * @return Response|array
     */
    protected function index(
        Request $request,
        string $view,
        string $pageName = '',
        int $page = 1,
        bool $isRendered = true
    ) {
        $tiles = $this->paginator->paginate(
            $this->getTargets($request, $pageName),
            $page,
            Tile::LIMIT_PER_PAGE
        );

        $params = [
            'tiles' => $tiles,
            'image' => Tile::TILE_IMAGE,
            'player' => Tile::TILE_PLAYER,
            'current_page' => $page,
            'previous_page' => $page - 1,
            'next_page' => $page + 1,
            'max_items' => Tile::LIMIT_PER_PAGE,
            'categories' => Page::PAGE_EDITION === $pageName ? $this->categoryManager->getAll() : null,
        ];

        if (!$isRendered) {
            return $params;
        }

        return $this->render($view, $params);
    }

    /**
     * @param Request $request
     * @param string $pageName
     * @return array
     */
    private function getTargets(Request $request, string $pageName): array
    {
        $category = $request->get('category');
        if (!empty($category)) {
            return $this->tileManager->getByCategoryName($category);
        }

        return !empty($pageName) ?
            $this->tileManager->getAllFromPage($pageName) :
            $this->tileManager->getAllOrdered();
    }
}
