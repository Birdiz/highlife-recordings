include docker/.docker.conf
export $(shell sed 's/=.*//' docker/.docker.conf)

DOCKER_COMPOSE=eval ${DOCKER_COMPOSE_CMD}

###########################
####### DEV COMMAND #######
###########################
init:
	@make restart
	@make install
	@make init-app
	@make encore
init-prod:
	@make restart-prod
	@make install
	@make init-app-prod
	@make encore
exec-php:
	${DOCKER_COMPOSE} exec php /bin/bash
install:
	${DOCKER_COMPOSE} exec php composer install
update:
	${DOCKER_COMPOSE} exec php composer update
init-app:
	${DOCKER_COMPOSE} exec php composer init-app
first-install:
	@make purge
	@make up
	@make config
	${DOCKER_COMPOSE} exec php composer install
	${DOCKER_COMPOSE} exec php composer init-app
	${DOCKER_COMPOSE} exec yarn install
	@make encore
###########################
####### DEV DOCKER ########
###########################
up:
	cp ./.env.docker ./.env
	${DOCKER_COMPOSE} up --build -d
up-prod:
	cp ./.env.prod ./.env
	${DOCKER_COMPOSE} up --build -d
config:
	${DOCKER_COMPOSE} exec -u root php groupadd localuser -g $$UID; true
	${DOCKER_COMPOSE} exec -u root php useradd localuser -g localuser --uid $$UID -d /home/localuser -m; true
	${DOCKER_COMPOSE} exec -u root php mkdir /home/localuser/.ssh; true
	${DOCKER_COMPOSE} exec -u root php cp -R /home/webuser/.ssh/ /home/localuser/; true
	${DOCKER_COMPOSE} exec -u root php chown -R localuser:localuser /home/localuser/.ssh; true
stop:
	${DOCKER_COMPOSE} stop
ps:
	${DOCKER_COMPOSE} ps
logs:
	${DOCKER_COMPOSE} logs -tf
purge:
	${DOCKER_COMPOSE} stop
	${DOCKER_COMPOSE} rm -f -v
restart:
	@make stop
	@make up
restart-prod:
	@make stop
	@make up-prod
encore:
	${DOCKER_COMPOSE} exec php yarn encore dev
encore-prod:
	${DOCKER_COMPOSE} exec php yarn encore prod
