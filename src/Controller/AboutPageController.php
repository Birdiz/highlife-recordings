<?php

namespace App\Controller;

use App\Entity\Page;
use App\Service\PageManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AboutPageController
 * @package App\Controller
 */
class AboutPageController extends AbstractController
{
    /** @var PageManager */
    private $pageManager;

    /**
     * AboutPageController constructor.
     * @param PageManager $pageManager
     */
    public function __construct(PageManager $pageManager)
    {
        $this->pageManager = $pageManager;
    }

    /**
     * @Route("/about", name="about_page")
     */
    public function index(): Response
    {
        /** @var Page $page */
        $page = $this->pageManager->getOneBy(['name' => Page::PAGE_ABOUT]);

        return $this->render('about_page/index.html.twig', [
            'content' => $page->getContent(),
        ]);
    }
}
