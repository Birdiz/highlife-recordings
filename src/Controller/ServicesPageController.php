<?php

namespace App\Controller;

use App\Entity\Page;
use App\Service\CategoryManager;
use App\Service\PageManager;
use App\Service\TileManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ServicesPageController
 * @package App\Controller
 */
class ServicesPageController extends PaginatedTilesController
{
    /** @var PageManager */
    private $pageManager;

    /**
     * ServicesPageController constructor.
     * @param PaginatorInterface $paginator
     * @param TileManager $tileManager
     * @param CategoryManager $categoryManager
     * @param PageManager $pageManager
     */
    public function __construct(
        PaginatorInterface $paginator,
        TileManager $tileManager,
        CategoryManager $categoryManager,
        PageManager $pageManager
    ) {
        parent::__construct($paginator, $tileManager, $categoryManager);

        $this->pageManager = $pageManager;
    }

    /**
     * @Route("/services/{page}", name="services_page", requirements={"page"="\d+"})
     * @param Request $request
     * @param string $view
     * @param string $pageName
     * @param int $page
     * @param bool $isRendered
     * @return Response
     */
    public function index(
        Request $request,
        string $view = 'services_page/index.html.twig',
        string $pageName = Page::PAGE_SERVICES,
        int $page = 1,
        bool $isRendered = true
    ): Response {
        $params = parent::index($request, $view, $pageName, $page, false);
        /** @var Page $pageServices */
        $pageServices = $this->pageManager->getOneBy(['name' => Page::PAGE_SERVICES]);

        $params['content'] = $pageServices->getContent();

        return $this->render($view, $params);
    }
}
