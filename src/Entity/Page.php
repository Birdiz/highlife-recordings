<?php

namespace App\Entity;

use App\Contract\EntityInterface;
use App\Validation\Constraints\Page as PageAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PageRepository")
 */
class Page implements EntityInterface
{
    public const PAGE_HOME = 'home';
    public const PAGE_ABOUT = 'about';
    public const PAGE_MUSIC = 'music';
    public const PAGE_EDITION = 'edition';
    public const PAGE_RADIO = 'radio';
    public const PAGE_CONTACT = 'contact';
    public const PAGE_SERVICES = 'services';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @PageAssert\InvalidName()
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tile", mappedBy="page", orphanRemoval=true)
     */
    private $tiles;

    /**
     * Page constructor.
     */
    public function __construct()
    {
        $this->tiles = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return $this
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection|Tile[]
     */
    public function getTiles(): Collection
    {
        return $this->tiles;
    }

    /**
     * @param Tile $tile
     * @return $this
     */
    public function addTile(Tile $tile): self
    {
        if (!$this->tiles->contains($tile)) {
            $this->tiles[] = $tile;
            $tile->setPage($this);
        }

        return $this;
    }

    /**
     * @param Tile $tile
     * @return $this
     */
    public function removeTile(Tile $tile): self
    {
        if ($this->tiles->contains($tile)) {
            $this->tiles->removeElement($tile);
            // set the owning side to null (unless already changed)
            if ($tile->getPage() === $this) {
                $tile->setPage(null);
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public static function getNamesForTile(): array
    {
        return [
            self::PAGE_HOME,
            self::PAGE_EDITION,
            self::PAGE_RADIO,
            self::PAGE_MUSIC,
            self::PAGE_SERVICES,
        ];
    }

    /**
     * @return array
     */
    public static function getPages(): array
    {
        return [
            self::PAGE_HOME,
            self::PAGE_ABOUT,
            self::PAGE_MUSIC,
            self::PAGE_EDITION,
            self::PAGE_RADIO,
            self::PAGE_CONTACT,
            self::PAGE_SERVICES,
        ];
    }

    /**
     * @return array
     */
    public static function getPagesEditable(): array
    {
        return [
            self::PAGE_ABOUT,
            self::PAGE_CONTACT,
            self::PAGE_SERVICES,
        ];
    }

    /**
     * @return array
     */
    public static function getPagesCaptionable(): array
    {
        return [
            self::PAGE_HOME,
            self::PAGE_EDITION,
            self::PAGE_SERVICES,
        ];
    }
}
