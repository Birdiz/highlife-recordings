<?php

namespace App\Contract;

/**
 * Interface EntityInterface
 * @package App\Contract
 */
interface EntityInterface
{
    public function getId(): ?int;
}
