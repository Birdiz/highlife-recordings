<?php

namespace App\Validation\Constraints\Tile;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class InvalidType extends Constraint
{
    public $message = 'The link must be an Image or a Player';
}
